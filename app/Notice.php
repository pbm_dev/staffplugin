<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    public static function makeHash() {

        return strtolower(str_random('16'));

    }

    public function user() {

        return $this->belongsTo('App\User');

    }

    public function device() {

        return $this->belongsTo('App\Device');

    }

    public function returnDays($arr) {

        $dayArray = unserialize($arr);

        $dayNamesArray = array();

        foreach ($dayArray as $day) {

            switch ($day) {

                case 1:
                    array_push($dayNamesArray, "Monday");
                    break;
                case 2:
                    array_push($dayNamesArray, 'Tuesday');
                    break;
                case 3:
                    array_push($dayNamesArray, 'Wednesday');
                    break;
                case 4:
                    array_push($dayNamesArray, 'Thursday');
                    break;
                case 5:
                    array_push($dayNamesArray, 'Friday');
                    break;
                case 6:
                    array_push($dayNamesArray, 'Saturday');
                    break;
                case 7:
                    array_push($dayNamesArray, 'Sunday');
                    break;

            }

        }

        return $dayNamesArray;

    }

    public function returnDayNumber(Carbon $now, $activity_start_at) {

        $currAct = Carbon::parse($activity_start_at);

        $startOfWeek = $now->startOfWeek()->subDay(1);

        for ($i = 0; $i <= 6; $i++) {

            $startOfDay = $startOfWeek->addDay(1)->startOfDay();
            $endOfDay   = clone $startOfDay;
            $endOfDay->endOfDay();

            if ($currAct->lte($endOfDay) && $currAct->gte($startOfDay)) {
                $index = $i + 1;
                return $index;
            }

        }

        return null;

    }

    public function makeItSimpleDate(Carbon $date) {

        $year   = $date->year;
        $month  = $date->month;
        $day    = $date->day;

        return $day.".".$month.".".$year.".";


    }
}
