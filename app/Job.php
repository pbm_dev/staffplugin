<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{

    use SoftDeletes;

    public static function checkForDuplicates($entry, $job_names) {

        foreach ($job_names as $name) {

            if (strtolower($entry) == strtolower($name)) {

                return true;
                break;

            }

            return false;

        }

    }

    public function user() {

        return $this->belongsTo('App\User');

    }

    public function staff() {

        return $this->belongsToMany('App\Staff', 'staff_job');

    }
}
