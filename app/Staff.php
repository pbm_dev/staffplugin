<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    public function user() {

        return $this->belongsTo('App\User');

    }


    public static function checkPassword($passwords, $passwordToCheck) {

        foreach ($passwords as $password) {

            if($passwordToCheck == $password) {

                return true;

                break;
            }

        }
        return false;

    }

    public function jobs() {

        return $this->belongsToMany('App\Job', 'staff_job');

    }

}
