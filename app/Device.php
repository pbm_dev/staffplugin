<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{

    use SoftDeletes;

    public function user() {

        return $this->belongsTo('App\User');

    }

    public static function makeHash() {

        return strtolower(str_random('16'));

    }


}
