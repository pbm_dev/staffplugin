<?php

namespace App;

use Carbon\Carbon;
use App\Device;
use App\Staff;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

    public function staff() {

        return $this->belongsTo('App\Staff');

    }

    public function job() {

        return $this->belongsTo('App\Job');

    }

    public static function checkLastActivity($job_id, $staff_id) {

        $activity = Activity::where('staff_id', $staff_id)->where('job_id', $job_id)->latest()->first();

        if($activity) {

            if (!$activity->end_at) {

                return true;

            }

            return false;

        } else {

            return false;
        }

    }

    public static function checkLastActivityFromStaff($staff_id) {

        $activity = Activity::where('staff_id', $staff_id)->where('end_at', null)->latest()->first();

        if($activity) {

            return true;

        } else {

            return false;

        }

    }

    public function makeItSimpleDate(Carbon $date) {

        $year   = $date->year;
        $month  = $date->month;
        $day    = $date->day;

        return $day.".".$month.".".$year.".";


    }

    public function returnDayNumber(Carbon $now, $activity_start_at) {

        $currAct = Carbon::parse($activity_start_at);

        $startOfWeek = $now->startOfWeek()->subDay(1);

        for ($i = 0; $i <= 6; $i++) {

            $startOfDay = $startOfWeek->addDay(1)->startOfDay();
            $endOfDay   = clone $startOfDay;
            $endOfDay->endOfDay();

            if ($currAct->lte($endOfDay) && $currAct->gte($startOfDay)) {
                $index = $i + 1;
                return $index;
            }

        }

        return null;

    }

    public static function collectData($device, $week) {

        $current_device     = Device::where('hash', $device)->firstOrFail();

            $current_device_id  = $current_device->id;
            $user_id = $current_device->user_id;

        $time = Carbon::now();

        if($week != 0) {

            $time->subWeeks($week);

        }

        $staffs = Staff::where('user_id', $user_id)->get();

        $resActivity = array();
        // making array of activities

        foreach ($staffs as $staff) {

            //change to now, in publish version -- change to some date to test

            $ct = clone $time;
            $sw = clone $time;
            $ew = clone $sw;

            $currentActivities = Activity::where('staff_id', $staff->id)
                ->where('start_at', '>=', $sw->startOfWeek()->startOfDay()->toDateTimeString())
                ->where('start_at', '<=', $ew->endOfWeek()->endOfDay()->toDateTimeString())
                ->get();


            foreach ($currentActivities as $activity) {

                $act_start_at = Carbon::parse($activity->start_at);

                array_push($resActivity,[
                    'id'           => $activity->id,
                    'day_index'    => $activity->returnDayNumber($ct, $act_start_at),
                    'title'        => $activity->job->name,
                    'type'         => 1,
                    'date'         => $activity->makeItSimpleDate(Carbon::parse($activity->start_at)),
                    'job_type'     => $activity->job->job_type,
                    'staff_name'   => $activity->staff->name,
                    'background_color' => '#333',
                    'font-color'   => 'black',
                ]);

            }

        }


        $rowCounter = array(0,0,0,0,0,0,0);

        //indexing array for calendar view

        foreach ($resActivity as $key => $item) {

            switch ($item['day_index']) {

                case 1:
                    $rowCounter[0]++;
                    array_push($resActivity[$key], array('row' => $rowCounter[0]));
                    break;
                case 2:
                    $rowCounter[1]++;
                    array_push($resActivity[$key], array('row' => $rowCounter[1]));
                    break;
                case 3:
                    $rowCounter[2]++;
                    array_push($resActivity[$key], array('row' => $rowCounter[2]));
                    break;
                case 4:
                    $rowCounter[3]++;
                    array_push($resActivity[$key], array('row' => $rowCounter[3]));
                    break;
                case 5:
                    $rowCounter[4]++;
                    array_push($resActivity[$key], array('row' => $rowCounter[4]));
                    break;
                case 6:
                    $rowCounter[5]++;
                    array_push($resActivity[$key], array('row' => $rowCounter[5]));
                    break;
                case 7:
                    $rowCounter[6]++;
                    array_push($resActivity[$key], array('row' => $rowCounter[6]));
                    break;
                default:
                    break;
            }

        }

        $gt_notice = clone $time;
        $lt_notice = clone $time;

        $notices_show_once = Notice::where('device_id', $current_device_id)->where('show_date', '>=', $gt_notice->startOfWeek()->startOfDay()->toDateString())
            ->where('show_date', '<=', $lt_notice->endOfWeek()->endOfDay()->toDateString())
            ->where('notice_type', 1)
            ->get();

        $notices_repeat = Notice::where('device_id', $current_device_id)->where('notice_type', 2)->get();

        foreach ($notices_show_once as $notice) {

            $ct = clone $time;

            array_push($resActivity, [
                'id'        => $notice->id,
                'day_index' => $notice->returnDayNumber($ct, $notice->show_date),
                'title' => $notice->title,
                'type' => 2,
                'date' => $notice->makeItSimpleDate(Carbon::parse($notice->show_date)),
                0 => [
                    'row' => $rowCounter[$notice->returnDayNumber($ct, $notice->show_date)-1]+1,
                ]
            ]);

            $rowCounter[$notice->returnDayNumber($ct, $notice->show_date)-1]++;

        }

        if ($notices_repeat) {

            foreach ($notices_repeat as $notice_repeat) {

                $ct_rpt = clone $time;

                $start_of_week = clone $ct_rpt;

                $day_arr = unserialize($notice_repeat->repeat_schedule);

                foreach ($day_arr as $day) {

                    $start_of_week->startOfWeek();

                    array_push($resActivity, [
                        'id'        => $notice_repeat->id,
                        'day_index' => (int) $day,
                        'title' => $notice_repeat->title,
                        'type' => 2,
                        'message' => $notice_repeat->message,
                        'date' => $notice_repeat->makeItSimpleDate(Carbon::parse($start_of_week->addDay($day-1))),
                        0 => [
                            'row' => $rowCounter[$day-1]+1,
                        ]
                    ]);

                    $rowCounter[$day-1]++;

                }

            }

        }

        $maxValueOfRows = max($rowCounter);

        $rowSorted = array();

        //order by row

        for ($i = 1; $i <= $maxValueOfRows; $i++) {

            $row = array();

            foreach ($resActivity as $acti) {

                if($acti[0]['row'] == $i) {

                    array_push($row, $acti);

                }

            }

            array_push($rowSorted, $row);
        }


        //order by day

        $finalFormat = array();

        foreach ($rowSorted as $row) {

            if($row) {

                $newRows = array();

                for ($i = 1; $i <= 7; $i++) {

                    $newRow = array();

                    foreach ($row as $day) {

                        if ($day['day_index'] == $i) {

                            $newRow[] = $day;

                        }

                    }

                    array_push($newRows, $newRow);

                }

            }
            array_push($finalFormat, $newRows);
        }

        return $finalFormat;

    }

}
