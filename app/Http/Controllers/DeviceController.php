<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\User;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = Device::all();

        $users = User::all();



        return view('super.device.devices')->with([

            'users' => $users,
            'devices' => $devices

        ]);
    }
     /**
        Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate( $request, [
            'street' => 'required',
            'zip' => 'required',
            'city' => 'required',
            'owner' => 'required',
        ]);

        $device = new Device();

        // Generating Hash, brale

        $device->street  = $request->input('street');
        $device->zip     = $request->input('zip');
        $device->city    = $request->input('city');
        $device->user_id = $request->get('owner');
        $device->hash    = strtolower(Device::makeHash());

        $device->save();

        return redirect('super/devices');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $device = Device::findOrFail($id);
        $user   = $device->user();
        $users  = User::pluck('name', 'id');

        return view('super.device.edit')->with([
            'device'       => $device,
            'user_owner'   => $user,
            'users'        => $users
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $device = Device::findOrFail($id);
//
        $this->validate( $request, [
            'street' => 'required',
            'zip' => 'required',
            'city' => 'required',
            'user_id' => 'required',
        ]);

        $device->update($request->all());

        return redirect('/super/devices');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $device = Device::findOrFail($id);

        $device->delete();

        return redirect('super/devices');
    }
}
