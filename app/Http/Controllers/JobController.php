<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Staff;
use App\User;
use App\Job;

class JobController extends Controller
{

    public function index() {

        $jobs = Job::all();
        $users  = User::all();

        return view('super.job.jobs')->with([

            'jobs' => $jobs,
            'users'  => $users,

        ]);

    }

    public function store(Request $request) {

        $this->validate( $request, [
            'name' => 'required',
            'user' => 'required',
        ]);

        $errors = new MessageBag();

        $job = new Job();
        $job_names = Job::where('user_id', $request->input('user'))->pluck('name')->toArray();


        if(Job::checkForDuplicates($request->input('name'), $job_names)) {

            $errors->add('same-job', "You want to add job that you as user already created, please concentrate");

            return back()->withErrors($errors);

        } else {

            $job->name        = $request->input('name');
            $job->user_id     = $request->input('user');

            $job->save();

            return redirect('/super/job');

        }

    }

    public function edit($id) {

        $job = Job::findOrFail($id);

        return view('super.job.edit')->with([
            'job' => $job,
        ]);
    }

    public function update(Request $request, $id) {

        $job = Job::findOrFail($id);
        $errors = new MessageBag();

        $this->validate($request, [

            'name' => 'required',
            'password' => 'required',

        ]);

        $job_names = Job::where('user_id', $request->input('user'))->pluck('name')->toArray();

        if(Job::checkForDuplicates($request->input('name'), $job_names)) {

            $errors->add('same-job', "You want to add job that you as user already created, please concentrate");

            return back()->withErrors($errors);

        } else {

            $job->name        = $request->input('name');

            $job->save();

            return redirect('/super/job');

        }

    }

    public function destroy($id) {

        $job = Job::findOrFail($id);

        $job->delete();

        return redirect("super/job");

    }

}
