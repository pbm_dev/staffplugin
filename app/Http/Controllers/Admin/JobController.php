<?php

namespace App\Http\Controllers\admin;

use App\Job;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    public function index() {

        $jobs = Job::where('user_id', session()->get('user_id'))->whereNull('deleted_at')->get();

        return view('admin.jobs.index')->with([
            'jobs' => $jobs,
        ]);

    }
}
