<?php

namespace App\Http\Controllers\admin;

use App\Device;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeviceController extends Controller
{
    public function index(Request $request) {

        $devices = Device::where('user_id', $request->session()->get('user_id'))->whereNull('deleted_at')->get();

        return view('admin.devices.index')->with([
            'devices' => $devices,
        ]);
    }

    public function store(Request $request) {

        $validator = $request->validate([
            'street' => 'required',
            'zip'    => 'required',
            'city'   => 'required',
        ]);


        $device = new Device();
        $device->user_id = $request->session()->get('user_id');
        $device->street  = $request->street;
        $device->zip     = $request->zip;
        $device->city    = $request->city;
        $device->hash    = strtolower(Device::makeHash());

        $device->save();

        return json_encode([
            'success' => 'The device is succesfully added!',
        ]);
    }

    public function destroy($id) {

        $device = Device::findOrFail($id);
        $device->delete();

        return redirect(route('admin.devices'));
    }

    public function edit($id) {

        $device = Device::findOrFail($id);

        return json_encode([
            'id'     => $device->id,
            'street' => $device->street,
            'zip'    => $device->zip,
            'city'   => $device->city,
        ]);

    }

    public function pushEdit(Request $request) {

        $device = Device::findOrFail($request->id);

        if(session()->get('user_id') == $device->user->id) {

            $device->street = $request->street;
            $device->zip    = $request->zip;
            $device->city   = $request->city;

            $device->save();

            return json_encode([
                'success' => "Svaka cast, proslo je",
            ]);

        } else {
            abort(404);
        }

    }

}
