<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login() {
        return view('auth.login');
    }

    public function authUser(Request $request) {
        if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {

            $user = Auth::user();
            $user_role = $user->role_id;

            if($user_role == 1) {

                return redirect()->route('super.jobs');


            } else if($user_role == 2) {

                //Storing User ID in Session, maan
                $request->session()->put('user_id',$user->id);
                return redirect()->route('admin.dashboard');

            } else {

                return redirect()->route('login');
            }
        }
    }
}
