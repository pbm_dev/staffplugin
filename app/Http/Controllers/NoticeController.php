<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Device;
use Illuminate\Http\Request;
use App\Notice;
use App\User;

class NoticeController extends Controller
{

    public function index() {

        $notices    = Notice::all();
        $users      = User::all();

        return view('super.notice.notice')->with([
           'users'      => $users,
           'notices'    => $notices,
        ]);

    }

    public function getDeviceFromUser($id) {

        $devices = Device::where('user_id', $id)->get();

//        return json_encode($devices);

        return $devices;
    }

    public function store(Request $request) {

        $this->validate($request, [
            'title'         => 'required',
            'message'       => 'required',
            'date'          => 'required',
            'repeat-notice' => 'required',
            'user'          => 'required',
            'device'        => 'required',
        ]);


        $notice = new Notice();

        $notice->title       = $request->input('title');
        $notice->message     = $request->input('message');
        $notice->user_id     = $request->input('user');
        $notice->device_id   = $request->input('device');
        $notice->show_date   = $request->input('date');
        $notice->notice_type = $request->input('repeat-notice');
        if($request->input('repeat-notice') == 1) {
            $notice->repeat_schedule = null;
        } elseif($request->input('repeat-notice') == 2) {
            $notice->repeat_schedule = serialize($request->get('repeat-day'));
        }

        $notice->save();

        return redirect('super/notice');

    }
}
