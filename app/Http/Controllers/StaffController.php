<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Staff;
use App\User;
use App\Job;

class StaffController extends Controller
{

    public function index() {

        $staffs = Staff::all();
        $users  = User::all();

        return view('super.staff.staffs')->with([

            'staffs' => $staffs,
            'users'  => $users,

        ]);

    }

    public function getJobsFromUser($id) {

        $jobs = Job::where('user_id', $id)->pluck('name', 'id');

        return json_encode($jobs);

    }

    public function store(Request $request) {

        $this->validate( $request, [
            'name' => 'required',
            'password' => 'required',
            'user' => 'required',
            'job' => 'required',
        ]);

        $errors = new MessageBag();

        $staff = new Staff();
        $passwords = Staff::where('user_id', $request->input('user'))->pluck('password')->toArray();


        if(Staff::checkPassword($passwords, $request->input('password'))) {

            $errors->add('same-password', "Password already exists for other users that you created");

            return back()->withErrors($errors);

        } else {

            $staff->name        = $request->input('name');
            $staff->password    = $request->input('password');
            $staff->user_id     = $request->input('user');

            $staff->save();

            foreach ($request->input('job') as $key => $value) {

                $staff->jobs()->attach($value);

            }


            return redirect('/super/staff');

        }

    }

    public function edit($id) {

        $staff = Staff::findOrFail($id);
        $jobs  = Job::where('user_id', $staff->user->id)->pluck('name','id');

        return view('super.staff.edit')->with([
            'staff' => $staff,
            'jobs'  => $jobs,
        ]);
    }

    public function update(Request $request, $id) {

        $staff           = Staff::findOrFail($id);
        $errors          = new MessageBag();
        $jobsFromStaff   = Job::where('user_id', $staff->user->id)->pluck('id');

        $this->validate($request, [

            'name' => 'required',
            'password' => 'required',
            'job' => 'required',

        ]);

        $passwords = Staff::where('user_id', $staff->user->id)->pluck('password')->toArray();

        if($request->input('password') == $staff->password) {

            $staff->name        = $request->input('name');
            $staff->password    = $request->input('password');

            $staff->save();

            $staff->jobs()->sync($request->input('job'));


            return redirect('/super/staff');

        } else {

            if(Staff::checkPassword($passwords, $request->input('password'))) {

                $errors->add('same-password', "Password already exists for other users that you created");

                return back()->withErrors($errors);

            } else {

                $staff->name        = $request->input('name');
                $staff->password    = $request->input('password');

                $staff->save();

                $staff->jobs()->sync($request->input('job'));


                return redirect('/super/staff');

            }

        }

    }

    public function destroy($id) {

        $staff = Staff::findOrFail($id);

        $staff->delete();
        $staff->jobs()->detach();

        return redirect("super/staff");

    }


}
