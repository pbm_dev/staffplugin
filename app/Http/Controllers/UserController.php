<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function index() {

        $users = User::all();

        return view('super.users.users')->with(['users' => $users]);


    }

    public function store(Request $request) {

        $this->validate( $request, [
            'companyName' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);

        $user = new User();

        $user->name = $request->input('companyName');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->role_id = 2;

        $user->save();

        return redirect('/super/users');

    }

    public function edit($id) {

        $user = User::findOrFail($id);

        return view('super.users.edit')->with([
            'user' => $user
        ]);

    }

    public function update(Request $request, $id) {

        $this->validate( $request, [
            'companyName' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = User::findOrFail($id);

        $user->name = $request->input('companyName');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));

        $user->save();

        return redirect('/super/users');

    }

    public function destroy($id) {

        $user = User::findOrFail($id);

        if($user) {
            $user->delete();
            return redirect('/super/users');
        }

    }

}
