<?php

namespace App\Http\Controllers;

use App\Notice;
use Illuminate\Http\Request;
use App\Device;
use App\Staff;
use App\Job;
use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\MessageBag;
use App\Activity;
use Carbon\Carbon;



class FrontDeviceController extends Controller
{
    public function show($device, $week) {

        $finalFormat        = Activity::collectData($device, $week);
        $current_device     = Device::where('hash', $device)->get();

        $nextweek = null;
        $previousweek = null;

        switch ($week) {
            case 0:
                $nextweek = null;
                $previousweek = 1;
                break;
            case 1:
                $nextweek = 0;
                $previousweek = 2;
                break;
            case 2:
                $nextweek = 1;
                $previousweek = 3;
                break;
            case 3:
                $nextweek = 2;
                $previousweek = 4;
                break;
            case 4:
                $nextweek = 3;
                $previousweek = null;
                break;
            default:
                $nextweek = null;
                $previousweek = null;
                break;
        }

        return view('front-device')->with([
            'res_activity' => $finalFormat,
            'device'       => $current_device,
            'next'         => $nextweek,
            'previous'     => $previousweek,
        ]);

    }

    public function store(Request $request, $device) {


        $currentDevice = Device::where('hash', $device)->get();
        $currentDevice_id = $currentDevice[0]->user->id;

        if ($request->password) {

            Session::flush();

            $currentDevice_passwords = Staff::where('user_id', $currentDevice_id)->pluck('password', 'id');

            $passwordCheck = false;

            foreach ($currentDevice_passwords as $key => $password) {

                if ($request->password == $password) {

                    $passwordCheck = true;
                    $currentStaff = $key;

                    Session::put('staff', $currentStaff);

                } else {

                    $passwordCheck = false;

                }

            }

            if ($passwordCheck) {

                $currentStaff_jobs = Staff::findOrFail($currentStaff)->jobs;
                $res = array();

                foreach ($currentStaff_jobs as $job) {

                    if(Activity::checkLastActivity($job->id, $currentStaff)) {

                        array_push($res, [
                            'id' => $job->id,
                            'name' => $job->name,
                            'unfinished' => 1,
                        ]);

                    } else {

                        array_push($res, [
                            'id' => $job->id,
                            'name' => $job->name,
                            'type' => $job->job_type,
                        ]);

                    }

                }

                return json_encode($res);

            } else {

                $res = array(

                    'error' => [
                        'password' => 'Wrong password, try again',
                    ],

                );

                return json_encode($res);

            }


        }

        if($request->job_id) {

            if($request->session()->has('staff')) {

                $job_type = Job::findOrFail($request->job_id)->job_type;

                $current_device = Device::where('hash', $device)->firstOrFail();

                $current_staff  = Session::get('staff');

                $activity = new Activity();

                if ($job_type == 1) {

                    $activity->start_at = Carbon::now();
                    $activity->end_at   = Carbon::now();

                    $activity->job_id   = $request->job_id;
                    $activity->staff_id = $current_staff;

                    $activity->save();

                    Session::flush();

                    $res = array([
                       'message' => 'Your work has been recorded!'
                    ]);

                    return json_encode($res);

                } elseif ($job_type == 2) {

                    if(Activity::checkLastActivity($request->job_id, $current_staff)) {

                        $activity_completiotion = $activity = Activity::where('staff_id', $current_staff)->where('job_id', $request->job_id)->latest()->first();
                        $activity_completiotion->end_at = Carbon::now();

                        $activity_completiotion->save();

                        Session::flush();

                        $res = array([
                            'message' => 'You work is signed out! Good bay!'
                        ]);

                        return json_encode($res);


                    } else {

                        $activity->start_at = Carbon::now();
                        $activity->job_id   = $request->job_id;
                        $activity->staff_id = $current_staff;

                        $activity->save();

                        Session::flush();

                        $res = array([
                            'message' => 'Your work has started! DONT FORGET TO SIGNOUT!'
                        ]);

                        return json_encode($res);


                    }

                }



            } else {

                abort(404);

            }




        }

    }

    public function getInfoForModal($id) {


        $notice = Notice::findOrFail($id);

        if ($notice) {

            $res = array(
                'title' => $notice->title,
                'message' => $notice->message,
            );
        }

        return json_encode($res);
    }

}
