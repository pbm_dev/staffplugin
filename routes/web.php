<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
//Route::get('/', 'HomeController@index')->name('home');
Route::get('/login', 'LoginController@login')->name('login');
Route::post('/login', 'LoginController@authUser')->name('auth-user');


 //Device -> Show!

Route::group(['prefix' => 'front'], function () {

    Route::get('/{device}/{week}', 'FrontDeviceController@show')->name('front-device.show');
    Route::post('/device/{device}', 'FrontDeviceController@store')->name('front-device.store');
    Route::get('/notice/get/{id}', 'FrontDeviceController@getInfoForModal')->name('front-device.getnotice');

});


////--> Super
//


Route::group(['prefix' => 'super', 'middleware' => 'supercheck'], function() {

//--+ Super
//  |
//  +-> Jobs

    Route::get('/job', 'JobController@index')->name('super.jobs');
    Route::post('/job/', 'JobController@store');
    Route::get('/job/{id}/edit', 'JobController@edit')->name('super.jobs.edit');
    Route::post('/job/{id}/edit', 'JobController@update');
    Route::get('/job/{id}/delete', 'JobController@destroy')->name('super.jobs.delete');
//--+ Super
//  |
//  +-> Users
    Route::get('users', 'UserController@index')->name('super.users');
    Route::post('users/', 'UserController@store');
    Route::get('users/{id}/edit', 'UserController@edit')->name('super.users.edit');
    Route::post('users/{id}/edit', 'UserController@update');
    Route::get('users/{id}/delete', 'UserController@destroy')->name('super.users.delete');

    //--+ Super
    //  |
    //  +-> Devices

    Route::get('devices', 'DeviceController@index')->name('super.devices');
    Route::post('devices/', 'DeviceController@store');
    Route::get('devices/{id}/edit', 'DeviceController@edit')->name('super.devices.edit');
    Route::post('devices/{id}/edit', 'DeviceController@update');
    Route::get('devices/{id}/delete', 'DeviceController@destroy')->name('super.devices.delete');

    //--+ Super
    //  |
    //  +-> Staff

    Route::get('staff', 'StaffController@index')->name('super.staffs');
    Route::post('staff/', 'StaffController@store');
    Route::get('staff/get/{id}', 'StaffController@getJobsFromUser');
    Route::get('staff/{id}/edit', 'StaffController@edit')->name('super.staff.edit');
    Route::post('staff/{id}/edit', 'StaffController@update');
    Route::get('staff/{id}/delete', 'StaffController@destroy')->name('super.staff.delete');

    //--+ Super
    //  |
    //  +-> Notice

    Route::get('notice', 'NoticeController@index')->name('super.notices');
    Route::post('notice/', 'NoticeController@store');
    Route::get('notice/get/{id}', 'NoticeController@getDeviceFromUser');
    Route::get('notice/{id}/edit', 'NoticeController@edit')->name('super.notices.edit');
    Route::post('notice/{id}/edit', 'NoticeController@update');
    Route::get('notice/{id}/delete', 'NoticeController@destroy')->name('super.notices.delete');

});

Route::group(['prefix' => 'admin','middleware' => 'rolecheck'], function() {

    Route::get('/dashboard', 'admin\DashboardController@index')->name('admin.dashboard');

    Route::get('/devices', 'admin\DeviceController@index')->name('admin.devices');
    Route::post('/devices', 'admin\DeviceController@store')->name('admin.device.add');
    Route::get('/devices/{id}/delete', 'admin\DeviceController@destroy')->name('admin.device.delete');
    Route::get('/devices/{id}/edit', 'admin\DeviceController@edit')->name('admin.device.edit');
    Route::post('/devices/edit', 'admin\DeviceController@pushEdit')->name('admin.device.pushedit');


    Route::get('/jobs', 'admin\JobController@index')->name('admin.jobs');


});
