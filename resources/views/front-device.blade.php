<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PBM Solutions GmbH - Staff Plugin</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('css/style-front.css') }}">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    {{--Flaticons--}}
    <link rel="stylesheet" href="{{ URL::asset('css/font/flaticon.css') }}" type="text/css">

    {{--<link rel="stylesheet" href="{{ URL::asset('css/font-2/flaticon.css') }}" type="text/css">--}}

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">


</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-2 left-section">
            <i class="flaticon-left-chevron-1"></i>
        </div>
        <div class="col-8 center-section">
            <span class="display-date">One of three columns<span>
        </div>
        <div class="col-2 right-section">
            <i class="flaticon-right-chevron-1"></i>
        </div>
    </div>
</div>

<div class="table-container" >

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Montag</th>
                <th scope="col">Dienstag</th>
                <th scope="col">Mittwoch</th>
                <th scope="col">Donnerstag</th>
                <th scope="col">Freitag</th>
                <th scope="col">Samstag</th>
                <th scope="col">Sonntag</th>
            </tr>
        </thead>
        <tbody>

            @foreach($res_activity as $row)

                    <tr>
                        @foreach($row as $day)

                            @if($day)

                                @if($day[0]['type'] == 1)

                                    <td class="" style="background-color: #F7F7F7; color: #2657C1; border-left: #2657C1 solid 5px;" data-content="{{ $day[0]['id'] }}" data-type="{{ $day[0]['type'] }}">
                                        <span style="font-style: italic;margin-left: 15px; ">{{ $day[0]['staff_name'] }}</span><br><br>
                                        <span style="font-size: 1.5em;margin-left: 15px;">{{ $day[0]['title'] }}</span><br><br>
                                        <span class="badge badge-pill badge-success" style="font-size: 0.9em;margin-left: 15px;background-color: #2657C1; color: #F7F7F7;">{{ $day[0]['date'] }}</span>
                                    </td>

                                @elseif($day[0]['type'] == 2)

                                    <td class="notice" style="background-color: #E6F7F7; color: #44558F; border-left: #FFE495 solid 5px;" data-content="{{ $day[0]['id'] }}" data-type="{{ $day[0]['type'] }}">
                                        <span style="font-style: italic;margin-left: 15px; ">Obavestenje</span><br><br>
                                        <span style="font-size: 1.5em;margin-left: 15px;">{{ $day[0]['title'] }}</span><br><br>
                                        <span class="badge badge-pill badge-success" style="font-size: 0.9em;margin-left: 15px; background-color: #FFE495; color: #44558F;">{{ $day[0]['date'] }}</span>
                                        <span style="float: right; bottom: 30px;"><i class="flaticon-cursor"></i></span>
                                    </td>

                                @endif

                            @else

                                <td></td>

                            @endif

                        @endforeach
                    </tr>

            @endforeach


        </tbody>
    </table>

</div>

<div id="fixedButton" >
    <span class="flaticon-password"></span>
    <h4>Login</h4>
</div>

{{--Login modal--}}
<div class="modal fade bd-example-modal-sm" id="numpadLogin" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <form id="staffLogin">
                <div class="form-group">
                    <input class="form-control" id="passCode" type="password"  value="" readonly>
                </div>
            </form>

            <div class="row">
                <button type="button" id="number" class="btn btn-primary number">1</button>
                <button type="button" id="number" class="btn btn-primary number">2</button>
                <button type="button" id="number" class="btn btn-primary number">3</button>
            </div>
            <div class="row">
                <button type="button" id="number" class="btn btn-primary number">4</button>
                <button type="button" id="number" class="btn btn-primary number">5</button>
                <button type="button" id="number" class="btn btn-primary number">6</button>
            </div>
            <div class="row">
                <button type="button" id="number" class="btn btn-primary number">7</button>
                <button type="button" id="number" class="btn btn-primary number">8</button>
                <button type="button" id="number" class="btn btn-primary number">9</button>
            </div>
            <div class="row">
                <button type="button" id="submit" class="btn btn-primary"><i class="flaticon-checked"></i></button>
                <button type="button" id="number" class="btn btn-primary number">0</button>
                <button type="button" id="delete" class="btn btn-primary">C</button>
            </div>
            <div class="row">
                <button type="button" id="close" class="btn btn-primary">Close</button>
            </div>
    </div>
</div>
</div>

{{--Sign job modal--}}
<div class="modal fade" id="selectJob" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Select Job</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body job-container">
                <button type="button" data-value="'+value.id+'" class="btn btn-primary btn-lg btn-block job-activity">'+value.name+'</button>
                <hr>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{--Error modal--}}

<div class="modal fade bd-example-modal-lg error-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content modal-alert">

        </div>
    </div>
</div>

{{--Activity modal--}}

<div class="modal fade notice-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h5 class="modal-title notice-title"></h5>
            </div>
            <div class="modal-body notice-message">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

<!-- Popper.JS -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>


<script>

    $(document).ready(function() {

        console.log("Jedno veliko hvala zasluzuje Dragana Mirkovic za izradu ovog WebApp-a, za njeno stvaralastvo u 90tim godinama, teske godine za Srbiju, ali velike za turbo folk scenu");
        console.log('"Da umirem - blago tebi, zaplakao za mnom ne bi" - Dragana Mirkovic 1999 n.e');

        $('#fixedButton').click(function() {
            $('#numpadLogin').modal({
                show: true,
            });

            setTimeout(function() {
                if(($("#numpadLogin").data('bs.modal') || {})._isShown) {
                    console.log('Closing the modal...');
                    $('#numpadLogin').modal('hide');
                }

            }, 60000);

        });

        $('.number').click(function() {

            var self = $(this);

            var number = self.html();
            var code   = $("#passCode").val();

            code = code + + +number;

            $('#passCode').val(code);

        });


        $('#delete').click(function() {

            $('#passCode').val("");

        });

        $('#close').click(function() {
            $('#numpadLogin').modal('hide');
        });

        //click on activity , modal show

        $(".notice").click(function() {

            var self = $(this);

            var id = self.attr('data-content');
            console.log(id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('/') ."/front/notice/get/"}}" +id ,
                dataType: 'json',
                type: "GET",
                success: function(data) {

                    $('.notice-title').empty();
                    $('.notice-message').empty();

                    $('.notice-title').append(''+data.title);
                    $('.notice-message').append(
                        '<p>'+data.message+'</p>'
                    );

                    $('.notice-modal').modal({
                        show: true,
                    });
                },

                error: function(data) {
                    $('#passCode').val("");
                    $('#numpadLogin').modal('hide');

                    $('.modal-alert').empty();
                    $('.modal-alert').append(
                        '<div class="alert alert-danger" role="alert">Something terrible happend!</div>'
                    );

                    $('.error-modal').modal({
                        show: true,
                    });

                    setTimeout(function() {
                        $('.error-modal').modal('hide');
                    }, 5000);

                }
            });

        });

        //submiting form with passcode

        $('#submit').click(function(e) {

            e.preventDefault();

            var self = $(this);

            $.ajax({
                headers: {
                    'XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
               url: "/device/{{ $device[0]->hash }}" ,
               type: "POST",
               dataType: "json",
               data: {
                   _token : '{{ csrf_token() }}',
                   password: $('#passCode').val(),
               },
               success: function(data) {
                   $('#passCode').val("");

                   $('#numpadLogin').modal('hide');

                   $('.job-container').empty();

                   if (data.error) {
                       $('.modal-alert').empty();
                       $('.modal-alert').append(
                            '<div class="alert alert-danger" role="alert">' + data.error.password + '</div>'
                       );
                       $('.error-modal').modal({
                           show: true,
                           backdrop: 'static',
                           keyboard: false,
                       });
                       setTimeout(function() {
                           $('.error-modal').modal('hide');
                       }, 5000);
                   } else {

                       $.each(data, function(index, value) {
                           console.log(value);
                           if(!value.unfinished) {
                               $('.job-container').append(
                                   '<button type="button" data-value="'+value.id+'" class="btn btn-primary btn-lg btn-block job-activity">'+value.name+'</button>'
                               );
                           } else if(value.unfinished == 1) {
                               $('.job-container').append(
                                   '<hr>'+
                                   '<h5>Ovi poslovi su u toku</h5>'+
                                   '<button type="button" data-value="'+value.id+'" class="btn btn-danger btn-lg btn-block job-activity">'+value.name+'</button>'
                               );
                           }
                       });

                       $('#selectJob').modal({
                           show: true,
                       });

                       //closing modal after inactivity
                       setTimeout(function() {
                           if(($("#selectJob").data('bs.modal') || {})._isShown) {
                               console.log('Closing the modal...');
                               $('#selectJob').modal('hide');
                           }

                       }, 60000);
                   }

               },

                error: function(data) {
                   $('#passCode').val("");
                   $('#numpadLogin').modal('hide');

                   $('.modal-alert').empty();
                   $('.modal-alert').append(
                       '<div class="alert alert-danger" role="alert">Something terrible happend!</div>'
                   );

                   $('.error-modal').modal({
                       show: true,
                       backdrop: 'static',
                       keyboard: false,
                   });

                    setTimeout(function() {
                        $('.error-modal').modal('hide');
                    }, 5000);

               }
            });

        });


    });


    //job assign - job activity
    $(document).on('click', '.job-activity', function() {

            self = $(this);

            $.ajax({

                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/device/{{ $device[0]->hash }}" ,
                type: "POST",
                dataType: "json",
                data: {
                    job_id: self.attr('data-value'),
                },

                success: function(data) {
                    $('#selectJob').modal('hide');

                    $('.modal-alert').empty();
                    $('.modal-alert').append(
                        '<div class="alert alert-success" role="alert">'+ data[0].message +'</div>'
                    );

                    $('.error-modal').modal({
                        show: true,
                    });

                    //closing modal after inactivity
                    setTimeout(function() {
                        if(($(".error-modal").data('bs.modal') || {})._isShown) {
                            console.log('Closing the modal...');
                            $('.error-modal').modal('hide');
                        }

                    }, 60000);

                    setTimeout(function() {
                        $('.error-modal').modal('hide');
                        location.reload();
                    }, 5000);


                },

                error: function(data) {
                    $('#selectJob').modal('hide');

                    $('.modal-alert').empty();
                    $('.modal-alert').append(
                        '<div class="alert alert-danger" role="alert">Something terrible happend!</div>'
                    );

                    $('.error-modal').modal({
                        show: true,
                        backdrop: 'static',
                        keyboard: false,
                    });

                    setTimeout(function() {
                        $('.error-modal').modal('hide');
                    }, 5000);
                },

            });



    });

</script>

</body>

</html>