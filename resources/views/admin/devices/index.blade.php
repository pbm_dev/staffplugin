@extends('layouts.admin')

@section('section')

    <h1>Device</h1>
    <hr>
    @if($devices)
        <div class="table-container">
        <table class="table table-sm">
            <thead>
            <tr>
                <th scope="col">Street</th>
                <th scope="col">City</th>
                <th scope="col">Zip</th>
                <th scope="col">Options</th>
            </tr>
            </thead>
            <tbody>
            @foreach($devices as $device)

                <tr>
                    <td style="vertical-align: middle;">{{ $device->street }}</td>
                    <td style="vertical-align: middle;">{{ $device->city }}</td>
                    <td style="vertical-align: middle;">{{ $device->zip }}</td>
                    <td>
                        <button type="button" class="btn btn-primary btn-edit" data-id="{{ $device->id }}">Edit</button>
                        <button type="button" class="btn btn-info btn-url" data-url="{{ url('/') ."/front/".$device->hash."/0" }}">Url</button>
                        <a href="{{ route("admin.device.delete", ['id' => $device->id]) }}"><button type="button" class="btn btn-danger">Delete</button></a>
                    </td>
                </tr>

            @endforeach

            </tbody>
        </table>
    </div>
    @endif

    <button type="button" class="btn btn-primary button-add">Add Device</button>


    {{--Modal for Adding Device--}}
    <div class="modal fade modal-add-device" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Device</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="addDeviceForm" method="POST">

                        @csrf

                        <div class="form-group">
                            <label for="deviceStreet">Street</label>
                            <input type="text" name="street" class="form-control" placeholder="Dr.-Karl-Renner-Ring 3">
                        </div>
                        <div class="form-group">
                            <label for="zip">Zip</label>
                            <input type="text" name="zip" class="form-control"  placeholder="1010">
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" name="city" class="form-control"  placeholder="Wien">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary submit-add-device">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    {{--Modal for messages--}}

    <div class="modal fade modal-message" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-body-message">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    {{--Modal for messages--}}

    <div class="modal fade modal-url" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Url</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-body-message">
                    <input value="" class="form-control url-input" readonly>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    {{--Modal for Editing Device--}}
    <div class="modal fade modal-edit-device" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Device</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editDeviceForm" method="POST">

                        @csrf
                        <div class="form-group">
                            <label for="deviceId">ID</label>
                            <input type="text" name="id" class="form-control edit-id-input" readonly>
                        </div>
                        <div class="form-group">
                            <label for="deviceStreet">Street</label>
                            <input type="text" name="street" class="form-control edit-street-input" >
                        </div>
                        <div class="form-group">
                            <label for="zip">Zip</label>
                            <input type="text" name="zip" class="form-control edit-zip-input" >
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" name="city" class="form-control edit-city-input" >
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary submit-edit-device">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        $('.button-add').click(function() {
            $('.modal-add-device').modal('show');
        });

//      Handle click event on submit
        $('.submit-add-device').click(function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('admin.device.add') }}",
                dataType: 'json',
                type: "POST",
                data: {
                    street: $("input[name='street']").val(),
                    zip: $("input[name='zip']").val(),
                    city: $("input[name='city']").val()
                },
                success: function(data) {
                    if(data.success) {
                        $('.modal-add-device').modal('hide');

                        $('.modal-body-message').append("<p>" + data.success + "</p>");
                        $('.modal-message').modal('show');


                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    }

                    console.log(data.success)
                },

                error: function(data) {


                }
            });
        });

        $('.btn-url').click(function() {
            $('.url-input').val($(this).data('url'));
            $('.modal-url').modal('show');
        });

        $('.btn-edit').click(function() {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('/') }}" + "/admin/devices/" +$(this).data('id')+ "/edit",
                dataType: 'json',
                type: "GET",
                success: function(data) {

                    $(".edit-id-input").val(data.id),
                    $('.edit-street-input').val(data.street);
                    $('.edit-zip-input').val(data.zip);
                    $('.edit-city-input').val(data.city);

                    $('.modal-edit-device').modal('show');
                },

                error: function(data) {

                    $('.modal-body-message').empty();
                    $('.modal-body-message').append("<p>" + data + "</p>");
                    $('.modal-message').modal('show');

                }
            });

        });

        $('.submit-edit-device').click(function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('/') }}" + "/admin/devices/edit",
                dataType: 'json',
                type: "POST",
                data: {
                    id: $(".edit-id-input").val(),
                    street: $(".edit-street-input").val(),
                    zip: $(".edit-zip-input").val(),
                    city: $(".edit-city-input").val()
                },
                success: function(data) {
                    if(data.success) {
                        $('.modal-edit-device').modal('hide');

                        $('.modal-body-message').empty();
                        $('.modal-body-message').append("<p>" + data.success + "</p>");
                        $('.modal-message').modal('show');


                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    }
                },

                error: function(data) {

                    $('.modal-edit-device').modal('hide');


                    $('.modal-body-message').empty();
                    $('.modal-body-message').append("<p>Error!</p>");
                    $('.modal-message').modal('show');
                }
            });
        });

    </script>
@endsection