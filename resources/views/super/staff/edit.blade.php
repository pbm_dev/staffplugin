@extends('layouts.super')

@section('section')


    <div class="container">
        @if ($staff)
        <div class="card border-primary mb-3">
            <div class="card-header ">
                Edit Staff
            </div>
            <div class="card-body text-primary">
                <form id="editStaffForm" method="POST">

                    @csrf

                    <div class="form-group">
                        <label for="exampleInputPassword1">User id</label>
                        <input class="form-control" type="text" name="id" value="{{ $staff->id }}" readonly>

                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Name</label>
                        <input class="form-control" name="name" type="text" placeholder="" value="{{ $staff->name }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleFormControlInput1" value="{{ $staff->password }}">
                    </div>

                        @foreach($jobs as $key => $value)
                            <div class="form-check" style="margin-bottom: 5px;">
                                <input class="form-check-input" type="checkbox" name="job[]" value="{{ $key}}" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    {{ $value }}
                                </label>
                            </div>
                        @endforeach


                    <button type="submit" class="btn btn-primary" id="editStaff">Submit</button>
                </form>
            </div>
        </div>
        @endif
        <script>
            $(document).ready(function() {

                $('#editStaff').click(function() {
                    var name = $("input[name='name']").val();
                    var password = $("input[name='password']").val();

                    if(!name || !password) {
                        alert("Try again");
                    } else {
                        $('#editStaffForm').submit();
                    }
                });

            });

        </script>

    </div>
@endsection