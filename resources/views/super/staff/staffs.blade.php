@extends('layouts.super')

@section('section')

<div class="container">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card border-primary mb-3">
        <div class="card-header ">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Add Staff
            </button>
        </div>
        <div class="collapse" id="collapseExample">
            <div class="card-body text-primary">
                <form id="addStaffForm" method="POST">

                    @csrf

                    <div class="form-group">
                        <label for="staffName">Name</label>
                        <input class="form-control" name="name" type="text" >
                    </div>
                    <div class="form-group">
                        <label for="password">Password code</label>
                        <input type="password" inputmode="numeric" name="password" class="form-control"  >
                    </div>
                    @if($users)
                        <div class="form-group">
                            <label for="company">Owner</label>
                            <select class="form-control" name="user" id="user">
                                <option value="">Choose...</option>
                            @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="form-check" id="list-check" style="margin-bottom: 5px;">

                    </div>

                    <button type="submit" class="btn btn-primary" id="addStaff">Submit</button>

                </form>
            </div>
        </div>
    </div>

    @if($staffs)

        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Owner</th>
                <th>Job Types</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            @foreach($staffs as $staff)
                <tr id="{{$staff->id}}">
                    <td>{{ $staff->id }}</td>
                    <td>{{ $staff->name }}</td>
                    <td>{{ $staff->user->name }}</td>
                    <td>
                        <ul>
                            @foreach($staff->jobs as $job)

                                <li>{{ $job->name }}</li>

                            @endforeach
                        </ul>
                    </td>
                    <td>{{ $staff->created_at }}</td>
                    <td>{{ $staff->updated_at }}</td>
                    <td>
                        <a class="edit-user" href="{!! route('super.staff.edit', ['id' => $staff->id]) !!}"><button type="button" class="btn btn-primary btn-sm ">Edit</button></a>
                        <a class="delete-device" href="{!! route('super.staff.delete', ['id' => $staff->id]) !!}">
                            <button type="button" class="btn btn-danger btn-sm">Delete</button>
                        </a>
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#code" data-content="{{ $staff->password }}">
                            Pass code
                        </button>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

    @endif
</div>
        {{--Modal--}}
        <div class="modal fade" id="code" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Device url</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input id="passCode" type="text" class="form-control" value="" readonly style="text-align: center;">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {

            $('#addStaff').click(function() {
                var name = $("input[name='name']").val();
                var password = $("input[name='password']").val();
                var owner = $( "#user option:selected" ).val();

                console.log("addstaff");
                if(!name || !password || !owner) {
                    alert("Try again");
                } else {
                    $('#addStaffForm').submit();
                }
            });

            $(".btn-info").click(function() {

                var self = $(this);

                var code = self.attr("data-content");

                $("#passCode").val(code);

            });

            $("#user").on('change', function() {

                var self = $(this);

                console.log('Event fired');

                var userId = self.val();

                if(userId) {

                    $.ajax({
                        url: '/super/staff/get/'+userId,
                        type: 'GET',
                        dataType: 'json',
                        beforeSend: function() {

                            $('#list-check').css("visibility", "visible");

                        },

                        success: function(data) {

                            $("#list-check").empty();

                            $.each(data, function(key, value) {
                                $("#list-check").append('<div class="form-check"><input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="job[]" value="'+ key + '"> <label class="form-check-label" for="inlineCheckbox1">'+ value +' </label></div>');
                            });


                        },

                        complete: function() {

                            $('#list-check').css('visibility', '');

                        },
                    });

                } else {
                    $('#list-check').empty();
                }

            });

        });

    </script>
@endsection

