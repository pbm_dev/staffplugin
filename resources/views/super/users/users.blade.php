@extends('layouts.super')


@section('section')

    <div class="container">
        <div class="card border-primary mb-3">
            <div class="card-header ">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Add User
                </button>
            </div>
            <div class="collapse" id="collapseExample">
                <div class="card-body text-primary">
                    <form id="addUserForm" method="POST">

                        @csrf

                        <div class="form-group">
                            <label for="exampleInputPassword1">Company name</label>
                            <input class="form-control" name="companyName" type="text" placeholder="Company name">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>

                        <button type="submit" class="btn btn-primary" id="addUser">Submit</button>

                    </form>
                </div>
            </div>
        </div>

        @if($users)

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>Email</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($users as $user)
                    <tr id="{{$user->id}}">
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->created_at}}</td>
                        <td>{{$user->updated_at}}</td>
                        <td>
                            <a class="edit-user" href="{!! route('super.users.edit', ['id' => $user->id]) !!}"><button type="button" data-row="{{$user->id}}" class="btn btn-primary btn-sm ">Edit</button></a>
                            <a class="delete-user" href="{!! route('super.users.delete', ['id' => $user->id]) !!}">
                                <button type="button" class="btn btn-danger btn-sm">Delete</button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        @endif

    </div>

    {{--Modal--}}
    <div class="modal message" tabindex="-1" role="dialog">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="message-modal"></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-sm message" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">

            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {

            $('#addUser').click(function() {
                var name = $("input[name='companyName']").val();
                var email = $("input[name='email']").val();
                var password = $("input[name='password']").val();

                if(!name || !email || !password) {
                    alert("Try again");
                } else {
                    $('#addUserForm').submit();
                }
            });

            $('.delete-user').click(function() {

                $.ajax({
                    url: $(this).attr('href'),
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        $(".message-modal").text("Good job");
                        $(".message").modal('show');
                    },
                    error: function(e) {
                        $(".message-modal").text("Not so good job!")
                        $(".message").modal('show');
                    }
                })

            });


        });

    </script>

@endsection