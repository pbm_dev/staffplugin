@extends('layouts.super')

@section('section')

    <div class="container">
        <div class="card border-primary mb-3">
            <div class="card-header ">
                Edit User
            </div>
            <div class="card-body text-primary">
                <form id="editUserForm" method="POST">

                    @csrf

                    <div class="form-group">
                        <label for="exampleInputPassword1">User id</label>
                        <input class="form-control" type="text" name="id" value="{{ $user->id }}" readonly>

                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Company name</label>
                        <input class="form-control" name="companyName" type="text" placeholder="" value="{{ $user->name }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" value="{{ $user->email }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary" id="editUser">Submit</button>
                </form>
            </div>
        </div>

        <script>
            $(document).ready(function() {

                $('.editUser').click(function() {
                    var name = $("input[name='companyName']").val();
                    var email = $("input[name='email']").val();
                    var password = $("input[name='password']").val();

                    if(!name || !email || !password) {
                        alert("Try again");
                    } else {
                        $('#editUserForm').submit();
                    }
                });

            });

        </script>

    </div>
@endsection