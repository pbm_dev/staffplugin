@extends('layouts.super')

@section('section')

    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card border-primary mb-3">
            <div class="card-header ">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Add Device
                </button>
            </div>
            <div class="collapse" id="collapseExample">
                <div class="card-body text-primary">
                    <form id="addDeviceForm" method="POST">

                        @csrf

                        <div class="form-group">
                            <label for="deviceStreet">Street</label>
                            <input type="text" name="street" class="form-control" placeholder="Dr.-Karl-Renner-Ring 3">
                        </div>
                        <div class="form-group">
                            <label for="zip">Zip</label>
                            <input type="text" name="zip" class="form-control"  placeholder="1010">
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" name="city" class="form-control"  placeholder="Wien">
                        </div>

                        @if($users)
                            <div class="form-group">
                                <label for="company">Owner</label>
                                <select class="form-control" name="owner" id="owner">
                                @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        @endif
                        <button type="submit" class="btn btn-primary" id="addDevice">Submit</button>

                    </form>
                </div>
            </div>
        </div>

        @if($devices)

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Street</th>
                    <th>Zip</th>
                    <th>City</th>
                    <th>Owner</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($devices as $device)
                    <tr id="{{$device->id}}">
                        <td>{{ $device->id }}</td>
                        <td>{{ $device->street }}</td>
                        <td>{{ $device->zip }}</td>
                        <td>{{ $device->city }}</td>
                        <td>{{ $device->user->name }}</td>
                        <td>{{ $device->created_at }}</td>
                        <td>{{ $device->updated_at }}</td>
                        <td>
                            <a class="edit-user" href="{!! route('super.devices.edit', ['device' => $device->id]) !!}"><button type="button" data-row="{{$user->id}}" class="btn btn-primary btn-sm ">Edit</button></a>
                            <a class="delete-device" href="{!! route('super.devices.delete', ['device' => $device->id]) !!}">
                                <button type="button" class="btn btn-danger btn-sm">Delete</button>
                            </a>
                                <button type="button" class="btn btn-info btn-sm info" data-toggle="modal" data-target="#url" data-content="{{ $device->hash }}">
                                    Url
                                </button>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        @endif

    </div>

    {{--Modal--}}
    <div class="modal fade" id="url" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Device url</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input id="fullUrl" style="text-align: center;" type="text" class="form-control" value="" readonly>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')

    <script>
        $(document).ready(function() {

            var self = $(this);

            $('#addDevice').click(function() {
                var street = $("input[name='street']").val();
                var zip = $("input[name='zip']").val();
                var city = $("input[name='city']").val();
                var owner = $( "#owner option:selected" ).val();

                if(!street || !zip || !city || !owner) {
                    alert("Try again");
                } else {
                    $('#addDeviceForm').submit();
                }
            });

            $(".info").click(function() {

                var self = $(this);

                var hash = self.attr("data-content");
                var url  = "http://staff-plugin.test/" +hash;

                $("#fullUrl").val(url);

            });


        });

    </script>

@endsection
