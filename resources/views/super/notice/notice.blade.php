@extends('layouts.super')

@section('section')

    <div class="container">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card border-primary mb-3">
            <div class="card-header ">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Add Notice
                </button>
            </div>
            <div class="collapse" id="collapseExample">
                <div class="card-body text-primary">
                    <form id="addNoticeForm" method="POST">

                        @csrf

                        <div class="form-group">
                            <label for="noticeTitle">Notice title</label>
                            <input class="form-control" name="title" type="text" >
                        </div>
                        <div class="form-group">
                            <label for="noticeMessage">Notice message</label>
                            <input class="form-control" name="message" type="text" >
                        </div>
                        <div class="form-group">
                            <label for="noticeMessage">When to show (in case of repeat, enter the start day)</label>
                            <input data-date-format="dd/mm/yyyy" name="date" id="datepicker" readonly>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="repeat-notice" id="repeat-radio1" value="1" >
                            <label class="form-check-label" for="repeat-radio1">
                                No repeat
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="repeat-notice" id="repeat-radio2" value="2">
                            <label class="form-check-label" for="repeat-radio2">
                                Repeat weekly
                            </label>
                        </div>
                        <div class="checkbox-container">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="monday" name="repeat-day[]" value="1">
                                <label class="form-check-label" for="monday">Monday</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="tuesday" name="repeat-day[]" value="2">
                                <label class="form-check-label" for="tuesday">Tuesday</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="wednesday" name="repeat-day[]" value="3">
                                <label class="form-check-label" for="wednesday">Wednesday</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="thursday" name="repeat-day[]" value="4">
                                <label class="form-check-label" for="thursday">Thursday</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="friday" name="repeat-day[]" value="5">
                                <label class="form-check-label" for="friday">Friday</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="saturday" name="repeat-day[]" value="6">
                                <label class="form-check-label" for="saturday">Saturday</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="sunday" name="repeat-day[]" value="7">
                                <label class="form-check-label" for="sunday">Sunday</label>
                            </div>
                        </div>

                        @if($users)
                            <div class="form-group">
                                <label for="user">User</label>
                                <select class="form-control" name="user" id="user">
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="form-group" id="devices">
                            <label for="devices">Select device</label>
                            <select class="form-control" id="device-select-list" name="device">
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary" id="addNotice">Submit</button>

                    </form>
                </div>
            </div>
        </div>
    </div>

        @if($notices)


            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Message</th>
                    <th>User</th>
                    <th>Device</th>
                    <th>Show Date</th>
                    <th>Repeat Schedule</th>
                    <th>Notice Type</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                </tr>
                </thead>
                <tbody>

                @foreach($notices as $notice)
                    <tr id="{{$notice->id}}">
                        <td>{{ $notice->id }}</td>
                        <td>{{ $notice->title }}</td>
                        <td>{{ $notice->message }}</td>
                        <td>{{ $notice->user->name }}</td>
                        <td>{{ $notice->device->street }}</td>
                        <td>{{ $notice->show_date }}</td>
                        <td>
                            @if($notice->repeat_schedule)
                            <ul>
                                    @foreach($notice->returnDays($notice->repeat_schedule) as $day)
                                        <li>{{ $day }}</li>
                                    @endforeach

                            </ul>
                            @else
                            none
                            @endif

                        </td>
                        <td>
                            @if($notice->notice_type == 1)
                                <span class="badge badge-danger">No repeat</span>
                            @elseif($notice->notice_type == 2)
                                <span class="badge badge-warning">Repeat Weekly</span>

                            @endif

                        </td>
                        <td>{{ $notice->created_at }}</td>
                        <td>{{ $notice->updated_at }}</td>
                        {{--<td>--}}
                            {{--<a class="edit-user" href="{!! route('super.jobs.edit', ['id' => $job->id]) !!}"><button type="button" class="btn btn-primary btn-sm ">Edit</button></a>--}}
                            {{--<a class="delete-device" href="{!! route('super.jobs.delete', ['id' => $job->id]) !!}">--}}
                                {{--<button type="button" class="btn btn-danger btn-sm">Delete</button>--}}
                            {{--</a>--}}
                        {{--</td>--}}
                    </tr>
                @endforeach

                </tbody>
            </table>

        @endif
    <script>

        var today;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

        $(document).ready(function() {

            $('#addNotice').click(function() {
                var title = $("input[name='title']").val();
                var user = $( "#user option:selected" ).val();

                if(!name || !user) {
                    alert("Try again");
                } else {
                    $('#addNoticeForm').submit();
                }
            });

            $('#repeat-radio2').click(function() {
               if($(this).is(':checked')) {
                    $('.checkbox-container').show();
               }
            });

            $('#repeat-radio1').click(function() {
                if($(this).is(':checked')) {
                    $('.checkbox-container').hide();
                }
            });

            $("#user").on('change', function() {

                var self = $(this);

                console.log('Event fired');

                var userId = self.val();

                if(userId) {

                    $.ajax({
                        url: '/super/notice/get/'+userId,
                        type: 'GET',
                        dataType: 'json',
                        beforeSend: function() {

                            $('#devices').show();

                        },

                        success: function(data) {

                            $("#device-select-list").empty();
                            console.log(data);

                            $.each(data, function(key, value) {
                                $("#device-select-list").append('<option value="'+ value.id + '">'+ value.street +', '+value.zip +' '+ value.city +'</option>');
                            });


                        },

                        complete: function() {



                        },

                        error: function() {

                            $("#device-select-list").empty();
                            $('#devices').hide();


                        }
                    });

                } else {
                    $('#device-select-list').empty();
                }

            });




        });


        $('#datepicker').datepicker({
            minDate: today,
            uiLibrary: 'bootstrap4',
            locale: 'bg-bg',
            weekStartDay: 1,
            format: 'yyyy-mm-dd'
        });


    </script>

@endsection

