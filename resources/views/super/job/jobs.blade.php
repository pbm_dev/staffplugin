@extends('layouts.super')

@section('section')

    <div class="container">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card border-primary mb-3">
            <div class="card-header ">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Add Job
                </button>
            </div>
            <div class="collapse" id="collapseExample">
                <div class="card-body text-primary">
                    <form id="addJobForm" method="POST">

                        @csrf

                        <div class="form-group">
                            <label for="staffName">Job name</label>
                            <input class="form-control" name="name" type="text" >
                        </div>
                        @if(!empty($users))
                            <div class="form-group">
                                <label for="company">Owner</label>
                                <select class="form-control" name="user" id="user">
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <button type="submit" class="btn btn-primary" id="addJob">Submit</button>

                    </form>
                </div>
            </div>
        </div>

        @if(!empty($jobs))

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Owner</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($jobs as $job)
                    <tr id="{{$job->id}}">
                        <td>{{ $job->id }}</td>
                        <td>{{ $job->name }}</td>
                        <td>{{ $job->user->name }}</td>
                        <td>{{ $job->created_at }}</td>
                        <td>{{ $job->updated_at }}</td>
                        <td>
                            <a class="edit-user" href="{!! route('super.jobs.edit', ['id' => $job->id]) !!}"><button type="button" class="btn btn-primary btn-sm ">Edit</button></a>
                            <a class="delete-device" href="{!! route('super.jobs.delete', ['id' => $job->id]) !!}">
                                <button type="button" class="btn btn-danger btn-sm">Delete</button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        @endif
    </div>
    <script>
        $(document).ready(function() {

            $('#addJob').click(function() {
                var name = $("input[name='name']").val();
                var owner = $( "#user option:selected" ).val();

                if(!name || !owner) {
                    alert("Try again");
                } else {
                    $('#addJobForm').submit();
                }
            });

        });

    </script>

@endsection

