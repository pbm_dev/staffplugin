@extends('layouts.super')

@section('section')


    <div class="container">
        @if ($job)
        <div class="card border-primary mb-3">
            <div class="card-header ">
                Edit User
            </div>
            <div class="card-body text-primary">
                <form id="editJobForm" method="POST">

                    @csrf

                    <div class="form-group">
                        <label for="staffName">Job name</label>
                        <input class="form-control" name="name" type="text" value="{{ $job->name }}" >
                    </div>
                    <button type="submit" class="btn btn-primary" id="editJob">Submit</button>
                </form>
            </div>
        </div>
        @endif
        <script>
            $(document).ready(function() {

                $('#editJob').click(function() {
                    var name = $("input[name='name']").val();

                    if(!name) {
                        alert("Try again");
                    } else {
                        $('#editJobForm').submit();
                    }
                });

            });

        </script>

    </div>
@endsection